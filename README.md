
# Bitwave Strategy Module
  
These functionalities can be used in any project and include everything interesting of the Bitwave system, like
* viable Strategies
* Custom classes and functionality

This module will be included mostly as a Submodule for other git repos.
