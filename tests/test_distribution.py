import unittest
import os
from bwbackend.src.distribution import Distribution


class TestStringMethods(unittest.TestCase):

    def test_distribution_saves_data(self):
        d = Distribution({'btc': 1.0})
        self.assertEqual(d.distribution['btc'], 1.0)

if __name__ == '__main__':
    unittest.main()
