import unittest
import os
from bwbackend.src.distribution import Distribution
from bwbackend.src.position import Position
import pandas as pd
import bwbackend.src.utils as utils

class TestStringMethods(unittest.TestCase):

    def test_distribution(self):
        
        data = {}
        for m in utils.supported_markets:
            data[m] = pd.Series([])
            data[m]['timestamp'] = 1581274000
            data[m]['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
            data[m]['ask'] = 9426.3
            data[m]['bid'] = 9422.7
            data[m]['bidsize'] = 1
            data[m]['asksize'] = 1
            data[m]['volume'] = 1
            data[m]['lastprice'] = 1


        data['btcusd'] = pd.Series([])
        data['btcusd']['timestamp'] = 1581274000
        data['btcusd']['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
        data['btcusd']['ask'] = 9426.3
        data['btcusd']['bid'] = 9422.7
        data['btcusd']['bidsize'] = 1
        data['btcusd']['asksize'] = 1
        data['btcusd']['volume'] = 1
        data['btcusd']['lastprice'] = 1

        data['ethusd'] = pd.Series([])
        data['ethusd']['timestamp'] = 1581274000
        data['ethusd']['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
        data['ethusd']['ask'] = 501
        data['ethusd']['bid'] = 501
        data['ethusd']['bidsize'] = 1
        data['ethusd']['asksize'] = 1
        data['ethusd']['volume'] = 1
        data['ethusd']['lastprice'] = 1
        
        distribution = Distribution({'usd': 0.5, 'btc': 0.25, 'eth': 0.25})
        
        position = Position({})
        position.set_from_investment(amount_usd=100, 
                                     distribution=distribution, 
                                     market_data=data)
        
        self.assertTrue(position.get_distribution(market_data = data).equals(distribution))
    
    def test_value_is_the_same(self):
        
        data = {}
        for m in utils.supported_markets:
            data[m] = pd.Series([])
            data[m]['timestamp'] = 1581274000
            data[m]['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
            data[m]['ask'] = 9426.3
            data[m]['bid'] = 9422.7
            data[m]['bidsize'] = 1
            data[m]['asksize'] = 1
            data[m]['volume'] = 1
            data[m]['lastprice'] = 1
        data['btcusd'] = pd.Series([])
        data['btcusd']['timestamp'] = 1581273556
        data['btcusd']['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
        data['btcusd']['ask'] = 9426.3
        data['btcusd']['bid'] = 9422.7
        data['btcusd']['bidsize'] = 1
        data['btcusd']['asksize'] = 1
        data['btcusd']['volume'] = 1
        data['btcusd']['lastprice'] = 1

        data['ethusd'] = pd.Series([])
        data['ethusd']['timestamp'] = 1581274000
        data['ethusd']['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
        data['ethusd']['ask'] = 501
        data['ethusd']['bid'] = 501
        data['ethusd']['bidsize'] = 1
        data['ethusd']['asksize'] = 1
        data['ethusd']['volume'] = 1
        data['ethusd']['lastprice'] = 1
        
        distribution = Distribution({'usd': 0.5, 'btc': 0.25, 'eth': 0.25})
        
        position = Position({'usd': 1})
        position.set_from_investment(amount_usd=100, 
                                     distribution=distribution, 
                                     market_data=data)
        
        self.assertEqual(100, position.evaluate_position_value_in_usd(market_data=data))
        

if __name__ == '__main__':
    unittest.main()
