import unittest
import os
import bwbackend.src.utils as utils
import pandas as pd
class TestStringMethods(unittest.TestCase):

    def setUp(self):
        data = {}
        data['btcusd'] = pd.Series([])
        data['btcusd']['timestamp'] = 1581274000
        data['btcusd']['timestamp'] = pd.to_datetime(data['btcusd']['timestamp'], unit='s')
        data['btcusd']['ask'] = 9426.3
        data['btcusd']['bid'] = 9422.7
        data['btcusd']['bidsize'] = 1
        data['btcusd']['asksize'] = 1
        data['btcusd']['volume'] = 1
        data['btcusd']['lastprice'] = 1

        self.data = data

    
    def test_get_coin_price_correct(self):
        price = utils.get_coin_price('btcusd', self.data)
        self.assertEqual(price, (9426.3 + 9422.7) / 2)
        

if __name__ == '__main__':
    unittest.main()
