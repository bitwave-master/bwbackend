def bollinger_bands(df, price_col, windowsize, std_factor):
    """
    df: dataframe with price_col col in it
    price_col: name of the time column of df (type: str)
    window_size: number of rows to take into account for the moving average (type: int)
    std_factor: by how many times the std has to be mutliplied (type: int)
    
    return: 
    - original df with 3 new cols:
        - Moving average with moving average over windowsize size days (bollinger_band_mean_...)
        - upper bollinger band (bollinger_band_upper_...)
        - lower bollinger band (bollinger_band_lower_...)
    - names of df col with avg price, upper BB, lower BB (type: string)
    (type: dataframe)
    """
    
    col_name_mean = f"bollinger_band_mean_{windowsize}rows_{std_factor}std"
    col_name_upper = f"bollinger_band_upper_{windowsize}rows_{std_factor}std"
    col_name_lower = f"bollinger_band_lower_{windowsize}rows_{std_factor}std"
    
    df[col_name_mean] = df[price_col].rolling(window=windowsize).mean()
    df[col_name_upper] = df[col_name_mean] + std_factor * df[price_col].rolling(window=windowsize).std()
    df[col_name_lower] = df[col_name_mean] - std_factor * df[price_col].rolling(window=windowsize).std()

    return df, col_name_mean, col_name_upper, col_name_lower