import pandas as pd
import numpy as np
import sqlalchemy as db
import os
import sys

supported_coins = ["usd", "btc", "eos", "eth", "ltc", "neo", "xrp"]
supported_markets = ["btcusd","ethusd", "ltcusd","ethbtc", "xrpusd", "eosusd", "neousd"]

try:
    engine = db.create_engine(f"postgresql://{os.environ['BITWAVE_DATABASEUSER']}:\
        {os.environ['BITWAVE_DATABASEPW']}@\
        {os.environ['BITWAVE_DATABASEHOST']}:\
        {os.environ['BITWAVE_DATABASEPORT']}/\
        {os.environ['BITWAVE_DATABASENAME']}".replace(" ", "")) 
    print("Connection to DB successful!")
except:
    print("could not connect to Database! ")

def get_all_data(update_from_db=True):    
    def load_coin_lazy(data, name):
        save_path = os.path.join('data',name + ".csv")
        if not os.path.exists('data'):
            os.makedirs('data')
        if os.path.isfile(save_path):
            print('Loading ' + name + ' data from local file ' + save_path)
            data[name] = pd.read_csv(save_path)
        else:
            print('Loading ' + name + ' data from database and caching it...')
            data[name] = pd.read_sql_table(name, con=engine)
            data[name]['timestamp'] = pd.to_datetime(data[name]['timestamp'], unit='s')
        
        data[name] = data[name].sort_values(['timestamp'],ascending='False')
    
    all_data = {}
    for market_name in supported_markets:
        load_coin_lazy(all_data, market_name)
    if update_from_db:
        update_all_data(all_data, caching = True)
    
    for m in all_data:
        all_data[m].index = all_data[m]['timestamp']
    
    all_data = add_shorting_markets(all_data)
    return all_data

def add_shorting_markets(market_data):
    new_markets = {}
    for m in market_data:
        data = market_data[m]
        
        increases =  np.divide(np.array(data['bid'].iloc[1:]), np.array(data['bid'].iloc[:-1]))
        new_bid = [data['bid'].iloc[0]]
        for inc in increases:
            new_bid.append(new_bid[-1] * (1/inc))
            
        increases = np.divide(np.array(data['ask'].iloc[1:]), np.array(data['ask'].iloc[:-1]))
        new_ask = [data['ask'].iloc[0]]
        for inc in increases:
            new_ask.append(new_ask[-1] * (1/inc))
            
        new_shorting_market = data.copy()
        new_shorting_market['bid'] = new_bid
        new_shorting_market['ask'] = new_ask
        
        new_markets[m] = market_data[m]
        new_markets['s' + m] = new_shorting_market
        
    return new_markets
            
        

def get_all_data_between(data, start=pd.Timestamp(2017, 1, 1, 1),end=pd.Timestamp(2025, 1, 1, 1)):
    
    data_restricted = {}
    for m in data:
        data_restricted[m] = data[m][np.logical_and(start <= data[m]['timestamp'], end >= data[m]['timestamp'])]
        
    return data_restricted
                          

saved_requests = {}

def get_all_market_information(time, data):
    result = {}
    for m in data:
        result[m] = get_market_information(m,time,data)
    return data

def get_market_information(market_name, time, data):

    if market_name == "usdusd":
        return {"timestamp": 0, "bid": 1, "ask": 1, "bidsize": 1, "asksize": 1, "volume": 1, "lastprice": 1}
    # use the fact that we sort the data for fast access
    pos = data[market_name]['timestamp'].searchsorted(value = time) - 1
    if pos == -1:
        pos = 0
    return data[market_name].iloc[pos]
                          
def get_coin_price(market_name, data):
    if market_name == "usdusd":
        return 1
    else:
        return (data[market_name]['bid'] + data[market_name]['ask'])/2

def update_all_data(market_data, caching = False):

    for market_name in supported_markets:
        # update
        lastTimestamp = np.max(market_data[market_name]['timestamp'])

        # receive all new entries from DB
        df = pd.read_sql(f"SELECT * from {market_name} \
            where timestamp > {(lastTimestamp - pd.Timestamp('1970-01-01')).total_seconds()}", engine)
        df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
        market_data[market_name] = market_data[market_name].append(df, ignore_index=True)

        # cache results only when program starts
        if caching:
            market_data[market_name].to_csv(os.path.join("data", market_name + ".csv"))
    
    return market_data                     
    
