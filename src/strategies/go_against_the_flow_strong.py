import pandas as pd
from bwbackend.src.utils import get_coin_price
from bwbackend.src.distribution import Distribution

frequency = pd.Timedelta(minutes=1)
windows_size = pd.Timedelta(days=2)

def calculate_position_distribution(time, market_data):
    
    btc_price_usd_yesterday = get_coin_price('btcusd', time - pd.Timedelta(days=1), market_data)
    btc_price_usd = get_coin_price('btcusd', time, market_data)
    
    increase = (btc_price_usd - btc_price_usd_yesterday) / btc_price_usd_yesterday
    
    if increase >= 0.02:
        return Distribution({'usd': 1.0})
    elif increase <= -0.02:
        return Distribution({'btc': 1.0})
    else:
        return Distribution({'usd': 0.5, 'btc': 0.5})
