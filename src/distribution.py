import pandas as pd
import numpy as np
from bwbackend.src.utils import supported_coins

class Distribution():
    def __init__(self, starting_distribution):
        
        # correction for numerical errors
        added = np.sum([np.abs(starting_distribution[m]) for m in starting_distribution])
        if added < 1.0 - 1e-6 or added > 1.0:
            raise Exception("distribution has to add to 1!")
        elif added < 1.0:
            print("small numerical error detected... ")
            if 'usd' in starting_distribution:
                starting_distribution['usd'] += 1.0 - added
            else:
                starting_distribution['usd'] = 1.0 - added
        
        self.distribution = {}
        for m in supported_coins:
            self.distribution[m] = starting_distribution.get(m, 0.0)
        
    def equals(self, other) -> bool:
        for m in supported_coins:
            if other.distribution.get(m, 0.0) - self.distribution.get(m, 0.0) >= 1e-8:
                return False
        return True
    
    def to_df(self):
        result = pd.DataFrame()
        for m in supported_coins:
            result[m] = [self.distribution.get(m, 0.0)]
        return result
    
    def __str__(self):
        string = ""
        for m in supported_coins:
            string += f"{m}: {self.distribution.get(m, 0.0)}, "
        return string