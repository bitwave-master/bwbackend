import pandas as pd

def timebuilding(df):
    """
    Create new columm with yyyy-mm-dd hh:mm:00, i.e. all dates within the same minute will have the same date 

    :param df: df with columns year, month, day, hour, minute
    :return: df with new colum dteTme
    """
    time_datetime = pd.datetime(
        df.year,
        df.month,
        df.day,
        int(df.hour),
        int(df.minute)
    )

    return time_datetime

def timebuilding_hour(df):
    """
    Create new columm with yyyy-mm-dd hh:00:00, i.e. all dates within the same hour will have the same date 

    :param df: df with columns year, month, day, hour
    :return: df with new colum dteTme
    """
    time_datetime = pd.datetime(
        df.year,
        df.month,
        df.day,
        int(df.hour),
        int(0)
    )

    return time_datetime


def timebuilding_day(df):
    """
    Create new columm with yyyy-mm-dd 00:00:00, i.e. all dates within the same day will have the same date 

    :param df: df with columns year, month, day
    :return: df with new colum dteTme
    """
    time_datetime = pd.datetime(
        df.year,
        df.month,
        df.day,
        int(0),
        int(0)
    )

    return time_datetime


def timebuilding_month(df):
    """
    Create new columm with yyyy-mm-01 00:00:00, i.e. all dates within the same month will have the same date 

    :param df: df with columns year, month
    :return: df with new colum dteTme
    """
    time_datetime = pd.datetime(
        df.year,
        df.month,
        int(1),
        int(0),
        int(0)
    )

    return time_datetime

def add_timecols(df,time_col):

    """
    Wrapper for all the timebuilding functions
    Creates date columns covering year to minute groupby levels

    :param df: dataframe with col time_col of type datetime64[ns] (type: pandas DataFrame)
    :param time_col: name of the time column of df (type: str)

    :return: original df with 10 new date columns
    """

    df['date'] = pd.to_datetime(df[time_col],unit='s')

    df['year'] = df.date.dt.year
    df['month'] = df.date.dt.month
    df['day'] = df.date.dt.day
    df['hour'] = df.date.dt.hour
    df['minute'] = df.date.dt.minute

    df["dteTme"] = df.apply(timebuilding, axis=1)
    df["dteTmeHour"] = df.apply(timebuilding_hour, axis=1) 
    df["dteTmeDay"] = df.apply(timebuilding_day, axis=1) 
    df["dteTmeMonth"] = df.apply(timebuilding_month, axis=1)

    return df