import pandas as pd
import numpy as np
from bwbackend.src.distribution import Distribution
from bwbackend.src.utils import get_market_information, supported_coins, get_coin_price

class Position():
    
    def __init__(self, starting_position):
        self.coins = {}
        for m in supported_coins:
            self.coins[m] = starting_position.get(m,0.0)
    
    def set_from_investment(self, amount_usd, distribution, market_data):
        for m in supported_coins:
            if distribution.distribution.get(m,0.0) > 0.0:
                self.coins[m] = amount_usd*distribution.distribution.get(m,0.0) / get_coin_price(m + "usd", market_data)
            elif distribution.distribution.get(m,0.0) < 0.0:
                self.coins[m] = amount_usd*distribution.distribution.get(m,0.0) / get_coin_price("s" + m + "usd", market_data)
            else:
                self.coins[m] = 0.0
    
    def get_distribution(self, market_data):
        portfolio_value = self.evaluate_position_value_in_usd(market_data)
        d = {}
        for m in supported_coins:
            if self.coins.get(m,0.0) > 0.0:
                d[m] = self.coins.get(m,0.0) * get_coin_price(m + "usd", market_data) / portfolio_value
            elif self.coins.get(m,0.0) < 0.0:
                d[m] = -1 * self.coins.get(m,0.0) * get_coin_price("s" + m + "usd", market_data) / portfolio_value
        return Distribution(d)    
        
    def evaluate_position_value_in_usd(self, market_data):
        portfolio_value = 0
        for m in supported_coins:
            if self.coins.get(m,0.0) > 0.0:
                portfolio_value += self.coins.get(m,0.0) * get_coin_price(m + "usd", market_data)
            elif self.coins.get(m,0.0) < 0.0:
                portfolio_value += -1 * self.coins.get(m,0.0) * get_coin_price("s" + m + "usd", market_data)
        return portfolio_value
    
    def __str__(self):
        string = ""
        for m in supported_coins:
            string += f"{m}: {self.coins.get(m,0.0)}, "
        return string